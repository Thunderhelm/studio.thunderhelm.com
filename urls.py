from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
import os
from . import views

urlpatterns =[
    path("", views.gallery, name="gallery"),
]

urlpatterns = urlpatterns + static("/studio-files/", document_root=str(settings.BASE_DIR) + "/studio-files/")
