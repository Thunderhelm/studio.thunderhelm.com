from django.db import models

# Create your models here.

class ShowcasedWork(models.Model):
    title = models.CharField(max_length=128)
    date = models.DateField()
    description = models.TextField()
    tags = models.JSONField()
    preview_image = models.ImageField(upload_to='studio-files/previews/', blank=True)
    full_file = models.FileField(upload_to='studio-files/fulls', blank=True)
    project_file = models.FileField(upload_to='studio-files/projects', blank=True)
