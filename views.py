from django.shortcuts import render, HttpResponse
from .models import ShowcasedWork

# Create your views here.

def gallery(request):
    works = ShowcasedWork.objects.all()
    return render(request, "gallery.html", {"works": works})
